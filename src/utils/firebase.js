import firebase from '@firebase/app';
import '@firebase/auth';
import '@firebase/firestore';

const firebaseConfig = {
  apiKey: "AIzaSyDYm4uiftn2c5tY5WDBtCrh7dyH2_sdKz0",
  authDomain: "budtracker-52a81.firebaseapp.com",
  projectId: "budtracker-52a81",
  storageBucket: "budtracker-52a81.appspot.com",
  messagingSenderId: "62205315629",
  appId: "1:62205315629:web:e52739abf2338a50cb06a4"
};

firebase.initializeApp(firebaseConfig);

const firestore = firebase.firestore();
const auth = firebase.auth();

export {
  firebase,
  firestore,
  auth,
}


